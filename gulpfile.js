var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

// js build
var distFolder = './src/assets';
var jsEntry = './src/app/browser.jsx';
var jsxSource = 'src/app/**/*.jsx';

gulp.task('build', function () {
    return browserify({entries: jsEntry, extensions: ['.jsx', '.js'], debug: true})
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(distFolder));
});

gulp.task('buildHello', function () {
    return browserify({entries: './src/app/hello.jsx', extensions: ['.jsx', '.js'], debug: true})
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('bundleHello.js'))
        .pipe(gulp.dest(distFolder));
});

gulp.task('watch', ['build'], function () {
    gulp.watch(jsxSource, ['build', 'buildHello']);
});

// styles
var sassFiles = './src/assets/**/*.scss';

gulp.task('styles', function(){
    console.log("rebuild styles");
    gulp.src(sassFiles)
        .pipe(concat("index.scss"))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(distFolder));
});

gulp.task('watch styles',function() {
    console.log("watch styles");    
    gulp.watch(sassFiles, ['styles']);
});

gulp.task('default', ['watch', 'watch styles']);