import express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import App from './app/app';
import template from './template';
import https from 'request-promise';

const server = express();

server.use('/assets', express.static('assets'));

server.get('/', (req, res) => {
  res.sendFile('index.html', {"root": __dirname});
});

server.get('/server', (req, res) => {

  let stateValue = { term: '', items: ['finish work', 'learn node'] };

  https.get('https://jsonplaceholder.typicode.com/comments', {json: true}).then((body)=> {    
    let responseTodos = body.slice(0, 10).map((val, index) => val.name);
    stateValue.items = stateValue.items.concat(responseTodos);
    
    res.send(renderTodo(stateValue));
  })
  .catch(err => {
    console.log(err);
    res.send(renderTodo(stateValue));
  });

});

 function renderTodo(stateValue) {
  const appString = renderToString(<App {...stateValue}/>);

  return template({
    body: appString,
    title: 'Hello World from the server',
    stateValue: JSON.stringify(stateValue)
  });
}


server.listen(8080);
console.log('listening on port 8080');
