import React from 'react';
import ReactDOM from 'react-dom';

export default class Hello extends React.Component {

  constructor(props)
  {
    super(props);
  }

  render() {
    return (
        <h1>hello world!</h1>
    );
  }
}

ReactDOM.render(<Hello />, document.getElementById('root'));