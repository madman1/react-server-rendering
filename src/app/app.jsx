import React, { Component } from 'react';
import List from './list';

export default class App extends Component {
  constructor(props) {
    super(props);

    if(typeof window === 'undefined') {
      this.state = {
        term: this.props.term,
        items: this.props.items
      };
    }
    else {
      this.state = window.__APP_INITIAL_STATE__;
    } 
  }

  onChange(event) {
    this.setState({ term: event.target.value });
  }

  onSubmit(event) {
    event.preventDefault();
    this.setState({
      term: '',
      items: [...this.state.items, this.state.term]
    });
  }

  render() {
    return (
      <div className="todo-container">
        <form onSubmit={this.onSubmit.bind(this)}>
          <input className="todo-input" value={this.state.term} onChange={this.onChange.bind(this)} />
          <button>Submit</button>
        </form>
        <List items={this.state.items} />
      </div>
    );
  }
}