export default ({ body, title, stateValue }) => {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>${title}</title>
        <link rel="stylesheet" href="assets/index.css" />
      </head>
      
      <body>
        <div id="root">${body}</div>
      </body>
      
      <script>window.__APP_INITIAL_STATE__ = ${stateValue}</script>
      <script src="assets/bundle.js"></script>
    </html>
  `;
};
